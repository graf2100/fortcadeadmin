import React, {Component}                                       from "react";
import Table                                                    from "../../../../components/Table/Table.jsx";
import {Button, Grid, Modal, TextField, Typography, withStyles} from '@material-ui/core';
import Complete                                                 from './Complete';

const styles = theme => ({
    paper: {
        position: 'absolute',
        width: '40%',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        margin: '5ch 30%',

    },
    button: {
        margin: theme.spacing.unit,
    },
    root: {
        overflow: 'auto'
    }
});

class Withdraw extends Component {

    constructor(props) {
        super(props);
        this.handelSubmit = this.handelSubmit.bind(this);
    }

    handelSubmit(e) {
        this.props.failedModalClose();
        e.preventDefault();
    }

    render() {
        let {data, classes, modal, modalClose, onFailed, failed, failedModalClose, select, handelChange} = this.props;
        return (<div>
                <Table tableHeaderColor="rose" tableHead={['Id', 'User', 'Withdrawal', 'Status', 'Date', '']}
                       tableData={data}/>
                <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description"
                       open={modal.open} onClose={modalClose} className={classes.root}>
                    <div className={classes.paper}>
                        <Complete onClose={modalClose} onFailed={onFailed} select={select}/>
                    </div>
                </Modal>
                <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description"
                       open={failed.open} onClose={failedModalClose}>
                    <div className={classes.paper}>
                        <Typography variant="title" gutterBottom>
                            {failed.title}
                        </Typography>
                        <form onSubmit={this.handelSubmit}>
                            <Grid
                                container
                                direction="row"
                                justify="flex-end"
                                alignItems="center">


                                <Grid item xs={12}>
                                    <TextField
                                        label="Message"
                                        fullWidth
                                        margin="normal"
                                        InputLabelProps={{
                                            shrink: true,
                                        }} value={failed.text} onChange={e => handelChange(e.target.value)}/>
                                </Grid>

                                <Grid item xs={12}>
                                    <Button variant="contained" type={'submit'} color="secondary">
                                        Send
                                    </Button>
                                </Grid>

                            </Grid>
                        </form>
                    </div>
                </Modal>
            </div>
        );
    }
}

export default withStyles(styles)(Withdraw);