import React, {Component} from "react";
import Table              from "../../../../components/Table/Table.jsx";

export default class Deposits extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {data} = this.props;
        return (<div><Table
            tableHeaderColor="rose"
            tableHead={['Id', 'User', 'Balance', 'Status', 'Date']}
            tableData={data}
        /></div>);
    }
}