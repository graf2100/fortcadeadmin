import React, {Component} from 'react';
import PropTypes          from 'prop-types';
import Grid               from '@material-ui/core/Grid';
import {withStyles}       from '@material-ui/core/styles';
import Card               from '@material-ui/core/Card';
import CardActions        from '@material-ui/core/CardActions';
import CardContent        from '@material-ui/core/CardContent';
import Button             from '@material-ui/core/Button';
import TextField          from '@material-ui/core/TextField';
import {withRouter}       from 'react-router-dom';
import image              from "../../assets/img/Fortnite2.jpg";
import Lock               from "@material-ui/icons/Lock";
import Snackbar           from "../../components/Snackbar/Snackbar.jsx";
import {connect}          from 'react-redux'
import {auth}             from "../../actions";

const styles = {
    card: {
        minWidth: 275,
        transform: 'translate3d(0, 0, 0)',
        transition: 'all 300ms linear',
        marginTop: '35vh'
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        marginBottom: 16,
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    textField: {
        width: '100%',
    },
    button: {
        justifyContent: 'center !important'
    },
    fortNite: {
        height: '100vh',
        backgroundImage: "url(" + image + ")",
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        position: 'absolute',
        left: 0,
        top: 0,
        zIndex: -2,
        width: "100%",
    },
    fillter: {
        height: '100vh',
        position: 'absolute',
        left: 0,
        top: 0,
        zIndex: -1,
        width: "100%",
        opacity: '.7',
        background: '#000'
    }
};

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: '',
            pass: '',
            notification: {
                message: '',
                show: false,
            }
        };
        this.submit = this.submit.bind(this);
        this.handelText = this.handelText.bind(this);
    }

    handelText(k, v) {
        this.setState({[k]: v})
    }

    submit(e) {
        const {login, pass} = this.state;
        if (!login || !pass) {
            this.showNotification('Please! Fill login and password!');

        }
        if (login === 'admin' && pass === 'admin') {
            this.props.dispatch(auth(login, pass));
            this.props.history.push('/users');
        } else {
            this.showNotification('Please! Сheck login and password!');
        }
        e.preventDefault();
    };

    alertTimeout;

    clearAlertTimeout() {
        if (this.alertTimeout !== null) {
            clearTimeout(this.alertTimeout);
        }
    }

    showNotification(message) {
        this.clearAlertTimeout();
        this.setState({notification: {show: true, message}});
        this.alertTimeout = setTimeout(
            function () {
                this.setState({notification: {show: false, message: ''}})
            }.bind(this),
            6000
        );
    }

    render() {
        const {classes} = this.props;
        const {login, pass, notification} = this.state;

        return (
            <div>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    <Card className={classes.card}>
                        <form onSubmit={this.submit}>
                            <CardContent>
                                <TextField
                                    id="login"
                                    label="Login"
                                    className={classes.textField}
                                    value={login}
                                    onChange={e => this.handelText('login', e.target.value)}
                                    margin="normal"
                                />
                                <TextField
                                    id="password-input"
                                    label="Password"
                                    className={classes.textField}
                                    type="password"
                                    value={pass}
                                    onChange={e => this.handelText('pass', e.target.value)}
                                    autoComplete="current-password"
                                    margin="normal"
                                />
                            </CardContent>
                            <CardActions className={classes.button}>
                                <Button type={'submit'} variant="contained" color="primary" size="large">
                                    Sign In
                                </Button>
                            </CardActions>
                        </form>
                    </Card>
                </Grid>
                <div className={classes.fortNite}>
                </div>
                <div className={classes.fillter}>
                </div>
                <Snackbar
                    place="tc"
                    color="danger"
                    icon={Lock}
                    message={notification.message}
                    open={notification.show}
                    closeNotification={() => this.setState({notification: {show: false, message: ''}})}
                    close
                />
            </div>
        );
    }
}

Login.propTypes = {
    classes: PropTypes.object.isRequired,
};
Login = connect()(Login);
export default withRouter(withStyles(styles)(Login));