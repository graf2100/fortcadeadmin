import React      from 'react';
import PropTypes  from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import Stepper    from '@material-ui/core/Stepper';
import Step       from '@material-ui/core/Step';
import StepLabel  from '@material-ui/core/StepLabel';
import Button     from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import EpicGames  from './EpicGames';
import Payment    from './Payment';

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 2,
        marginRight: theme.spacing.unit * 2,
        [theme.breakpoints.up(600 + theme.spacing.unit * 2 * 2)]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 3,
        marginBottom: theme.spacing.unit * 3,
        padding: theme.spacing.unit * 2,
        [theme.breakpoints.up(600 + theme.spacing.unit * 3 * 2)]: {
            marginTop: theme.spacing.unit * 6,
            marginBottom: theme.spacing.unit * 6,
            padding: theme.spacing.unit * 3,
        },
    },
    stepper: {
        padding: `${theme.spacing.unit * 3}px 0 ${theme.spacing.unit * 5}px`,
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing.unit * 3,
        marginLeft: theme.spacing.unit,
    },
});
const FailedArr = [{label: 'Sign in failed', massage: 'Why sign in failed?'}, {
    label: 'Payment failed',
    massage: 'Why is payment failing?'
}];
const steps = ['Sign in to Epicgames', 'Payment',];

class Checkout extends React.Component {
    state = {
        activeStep: 0,
    };

    handleNext = () => {
        const {activeStep} = this.state;
        if (activeStep === 1) {
            this.props.onClose();
        }
        else {
            this.setState({
                activeStep: activeStep + 1,
            });
        }
    };

    handleBack = () => {
        const {activeStep} = this.state;
        this.setState({
            activeStep: activeStep - 1,
        });
    };

    getStepContent(step) {
        let {user, sum} = this.props.select;
        switch (step) {
            case 0:
                return <EpicGames onFailed={this.props.onFailed} user={user}/>;
            case 1:
                return <Payment onFailed={this.props.onFailed} sum={sum}/>;
            case 2:
                return null;
            default:
                throw new Error('Unknown step');
        }
    }

    render() {
        const {classes, onFailed} = this.props;
        const {activeStep} = this.state;

        return (
            <React.Fragment>
                <Typography variant="display1" align="center">
                    Complete
                </Typography>
                <Stepper activeStep={activeStep} className={classes.stepper}>
                    {steps.map(label => (
                        <Step key={label}>
                            <StepLabel>{label}</StepLabel>
                        </Step>
                    ))}
                </Stepper>
                <React.Fragment>
                    <React.Fragment>
                        {this.getStepContent(activeStep)}

                        {activeStep !== 2 && <Button color="secondary"
                                                     onClick={() => onFailed(FailedArr[activeStep].massage)}>{FailedArr[activeStep].label}</Button>
                        }
                        <div className={classes.buttons}>


                            {activeStep !== 0 && (
                                <Button onClick={this.handleBack} className={classes.button}>
                                    Back
                                </Button>
                            )}
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={this.handleNext}
                                className={classes.button}
                            >
                                Done
                            </Button>
                        </div>
                    </React.Fragment>
                </React.Fragment>
            </React.Fragment>
        );
    }
}

Checkout.propTypes = {
    classes: PropTypes.object.isRequired,
    onFailed: PropTypes.func,
};

export default withStyles(styles)(Checkout);