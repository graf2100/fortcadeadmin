import React, {Component}                    from "react";
import Table                                 from "../../../../components/Table/Table.jsx";
import {Add, DeleteOutline, Edit}            from "@material-ui/icons";
import IconButton                            from '@material-ui/core/IconButton';
import Button                                from "../../../../components/CustomButtons/Button.jsx";
import {Grid, Modal, Typography, withStyles} from '@material-ui/core';
import CustomInput                           from "../../../../components/CustomInput/CustomInput.jsx";
import GridItem                              from "../../../../components/Grid/GridItem.jsx";
import io                                    from "socket.io-client";

const styles = theme => ({
        paper: {
            position: 'absolute',
            width: '40%',
            backgroundColor: theme.palette.background.paper,
            boxShadow: theme.shadows[5],
            padding: theme.spacing.unit * 4,
            margin: '10ch 30%'
        },
        button: {
            margin: theme.spacing.unit,
        }
    })
;

class CreditCart extends Component {
    socet = {};
    state = {
        open: false,
        modal: {
            titel: 'New credit card',
            inputs: [
                {md: 6, value: '', error: false, label: 'First name', type: 'first'},
                {md: 6, value: '', error: false, label: 'Last name', type: 'last'},
                {md: 9, value: '', error: false, label: 'Address', type: 'address'},
                {md: 3, value: '', error: false, label: 'City', type: 'city'},
                {md: 6, value: '', error: false, label: 'Province/State', type: 'province'},
                {md: 6, value: '', error: false, label: 'Zip code', type: 'zip'},
                {md: 9, value: '', error: false, label: 'Card', type: 'card'},
                {md: 3, value: '', error: false, label: 'Cvv', type: 'cvv'},
                {md: 6, value: '', error: false, label: 'Expiry', type: 'expiry'},
                {md: 6, value: '', error: false, label: 'Activation', type: 'activation'},
            ]
        },
        cards: [], editModal: {
            _id: '',
            open: false,
            titel: 'Edit credit card',
            inputs: [
                {md: 6, value: '', error: false, label: 'First name', type: 'first'},
                {md: 6, value: '', error: false, label: 'Last name', type: 'last'},
                {md: 9, value: '', error: false, label: 'Address', type: 'address'},
                {md: 3, value: '', error: false, label: 'City', type: 'city'},
                {md: 6, value: '', error: false, label: 'Province/State', type: 'province'},
                {md: 6, value: '', error: false, label: 'Zip code', type: 'zip'},
                {md: 9, value: '', error: false, label: 'Card', type: 'card'},
                {md: 3, value: '', error: false, label: 'Cvv', type: 'cvv'},
                {md: 6, value: '', error: false, label: 'Expiry', type: 'expiry'},
                {md: 6, value: '', error: false, label: 'Activation', type: 'activation'},
            ]
        }
    };

    constructor(props) {
        super(props);

        this.handleClose = this.handleClose.bind(this);
        this.handleEditClose = this.handleEditClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleModal = this.handleModal.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleEditSubmit = this.handleEditSubmit.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
    }

    handleClose() {
        this.setState({open: false});
    }

    handleEditClose() {
        this.setState({editModal: {
                _id: '',
                open: false,
                titel: 'Edit credit card',
                inputs: [
                    {md: 6, value: '', error: false, label: 'First name', type: 'first'},
                    {md: 6, value: '', error: false, label: 'Last name', type: 'last'},
                    {md: 9, value: '', error: false, label: 'Address', type: 'address'},
                    {md: 3, value: '', error: false, label: 'City', type: 'city'},
                    {md: 6, value: '', error: false, label: 'Province/State', type: 'province'},
                    {md: 6, value: '', error: false, label: 'Zip code', type: 'zip'},
                    {md: 9, value: '', error: false, label: 'Card', type: 'card'},
                    {md: 3, value: '', error: false, label: 'Cvv', type: 'cvv'},
                    {md: 6, value: '', error: false, label: 'Expiry', type: 'expiry'},
                    {md: 6, value: '', error: false, label: 'Activation', type: 'activation'},
                ]
            }});
    }

    handleModal() {
        this.setState({
                open: true, modal: {
                    titel: 'New credit card',
                    inputs: [
                        {md: 6, value: '', error: false, label: 'First name', type: 'first'},
                        {md: 6, value: '', error: false, label: 'Last name', type: 'last'},
                        {md: 9, value: '', error: false, label: 'Address', type: 'address'},
                        {md: 3, value: '', error: false, label: 'City', type: 'city'},
                        {md: 6, value: '', error: false, label: 'Province/State', type: 'province'},
                        {md: 6, value: '', error: false, label: 'Zip code', type: 'zip'},
                        {md: 9, value: '', error: false, label: 'Card', type: 'card'},
                        {md: 3, value: '', error: false, label: 'Cvv', type: 'cvv'},
                        {md: 6, value: '', error: false, label: 'Expiry', type: 'expiry'},
                        {md: 6, value: '', error: false, label: 'Activation', type: 'activation'},
                    ]
                }
            }
        );
    }

    componentDidMount() {
        this.socet = io();
        this.socet.on('connect', () => {
            this.socet.emit('getCreaditCart', 1);
        });
        this.socet.on('creaditCart', creaditCart => {
            let cards = [];
            creaditCart.forEach(({first, last, address, city, province, zip, card, cvv, expiry, activation, _id}) => {
                cards.push([first, last, address, city, province, zip, card, cvv, expiry, activation,
                    <IconButton
                        onClick={() => this.handleEdit(_id, [first, last, address, city, province, zip, card, cvv, expiry, activation])}>
                        <Edit/>
                    </IconButton>, <IconButton
                        onClick={() => this.handleRemove(_id, [first, last, address, city, province, zip, card, cvv, expiry, activation])}>
                        <DeleteOutline/>
                    </IconButton>])
            });
            this.setState({cards});
        });
    }

    handleChange(e, index) {
        let {modal} = this.state;
        modal.inputs[index].value = e.target.value;
        this.setState({modal})
    };

    handleEditChange(e, index) {
        let {editModal} = this.state;
        editModal.inputs[index].value = e.target.value;
        this.setState({editModal})
    };

    handleSubmit(e) {
        if (this.checkInputs()) {
            let obj = {};
            this.state.modal.inputs.forEach(input => {
                obj = {[input.type]: input.value, ...obj}
            });
            this.socet.emit('newCreaditCart', obj);
            this.setState({open: false});
        }
        e.preventDefault();
    };

    handleEditSubmit(e) {
        if (this.checkEditInputs()) {
            let {editModal} = this.state;
            let obj = {};
            editModal.inputs.forEach(input => {
                obj = {[input.type]: input.value, ...obj}
            });
            this.socet.emit('editCreaditCart', obj, editModal._id);
            editModal.open = false;
            this.setState({editModal});
        }
        e.preventDefault();
    };

    handleEdit(id, arr) {
        let {editModal} = this.state;
        editModal.open = true;
        editModal._id = id;
        editModal.inputs = editModal.inputs.map((input, index) => {
            input.value = arr[index];
            return input
        });
        this.setState({editModal})
    };

    handleRemove(id) {
        this.socet.emit('removeCreaditCart', id);
    };

    checkInputs() {
        let {modal} = this.state;
        let flag = true;
        modal.inputs = modal.inputs.map(input => {
            if (!input.value) {
                flag = false;
                input.error = true;
            } else {
                input.error = false;
            }
            return input;
        });
        this.setState({modal});
        return flag;
    }

    checkEditInputs() {
        let {editModal} = this.state;
        let flag = true;
        editModal.inputs = editModal.inputs.map(input => {
            if (!input.value) {
                flag = false;
                input.error = true;
            } else {
                input.error = false;
            }
            return input;
        });
        this.setState({editModal});
        return flag;
    }

    render() {
        let {open, modal, cards, editModal} = this.state;
        let {classes} = this.props;
        return (<div>
            <Button color="info" onClick={this.handleModal} round><Add/>New Card</Button>
            <Table
                tableHeaderColor="rose"
                tableHead={['First name', 'Last name', 'Address', 'City', 'Province/State', 'Zip code', 'Card', 'Cvv', 'Expiry', 'Activation', 'Edit', 'Delete']}
                tableData={cards}
            />
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={open}
                onClose={this.handleClose}
            >
                <div className={classes.paper}>
                    <Typography variant="title" id="modal-title">
                        {modal.titel}
                        <form onSubmit={this.handleSubmit}>
                            <Grid container>
                                {modal.inputs.map((input, index) => <GridItem xs={12} sm={12} md={input.md}
                                                                              key={input.label}>
                                    <CustomInput
                                        labelText={input.label}
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        error={input.error}
                                        inputProps={{
                                            value: input.value,
                                            onChange: e => this.handleChange(e, index)
                                        }}
                                    />
                                </GridItem>)}
                                <GridItem xs={12} sm={12} md={12}>
                                    <Button type="submit" color="success">Save Card</Button>
                                </GridItem>
                            </Grid>
                        </form>
                    </Typography>
                </div>
            </Modal>
            <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description"
                   open={editModal.open}
                   onClose={this.handleEditClose}>
                <div className={classes.paper}>
                    <Typography variant="title" id="modal-title">
                        {editModal.titel}
                        <form onSubmit={this.handleEditSubmit}>
                            <Grid container>
                                {editModal.inputs.map((input, index) => <GridItem xs={12} sm={12} md={input.md}
                                                                                  key={input.label}>
                                    <CustomInput
                                        labelText={input.label}
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        error={input.error}
                                        inputProps={{
                                            value: input.value,
                                            onChange: e => this.handleEditChange(e, index)
                                        }}
                                    />
                                </GridItem>)}
                                <GridItem xs={12} sm={12} md={12}>
                                    <Button type="submit" color="success">Save Card</Button>
                                </GridItem>
                            </Grid>
                        </form>
                    </Typography>
                </div>
            </Modal>
        </div>);
    }
};
CreditCart = withStyles(styles)(CreditCart);
export default CreditCart;