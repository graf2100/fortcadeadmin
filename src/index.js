import React                          from "react";
import ReactDOM                       from "react-dom";
import {createBrowserHistory}         from "history";
import {applyMiddleware, createStore} from 'redux';
import {Provider}                     from 'react-redux';
import thunk                          from 'redux-thunk';
import {createLogger}                 from 'redux-logger';
import {Route, Router, Switch}        from "react-router-dom";
import reducer                        from './reducer';
import indexRoutes                    from "./routes/index.jsx";
import "./assets/css/material-dashboard-react.css";

const middleware = [thunk];
middleware.push(createLogger());

const store = createStore(
    reducer,
    applyMiddleware(...middleware)
);

const hist = createBrowserHistory();

ReactDOM.render(
    <Provider store={store}>
        <Router history={hist}>
            <Switch>
                {indexRoutes.map((prop, key) => {
                    return <Route path={prop.path} component={prop.component} key={key}/>;
                })}
            </Switch>
        </Router>
    </Provider>,
    document.getElementById("root")
);
