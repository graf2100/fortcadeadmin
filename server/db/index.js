const {Users, Payments, Games, CreditCard, epicGamesAccount, myBets, PayPal} = require('./models');


module.exports = {Users, Payments, Games, CreditCard, epicGamesAccount, myBets, PayPal};