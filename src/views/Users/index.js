import React         from "react";
import GridItem      from "../../components/Grid/GridItem.jsx";
import GridContainer from "../../components/Grid/GridContainer.jsx";


import MyTabel from "./Tabels";


function Users() {
    return (
        <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
                <MyTabel/>
            </GridItem>
        </GridContainer>
    );
}

export default Users;
