import CreditCard from './CreditCard';
import Deposits   from './Deposits';
import Withdraw   from './Withdraw';
import PayPal     from './PayPal';


export {CreditCard, Deposits, Withdraw, PayPal}