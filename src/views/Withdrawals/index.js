import React, {Component}                                      from 'react';
import GridContainer                                           from "../../components/Grid/GridContainer.jsx";
import io                                                      from 'socket.io-client';
import {CreditCard as Icon, LocalAtm, MonetizationOn, ShopTwo} from "@material-ui/icons";
import Tabs                                                    from "../../components/CustomTabs/CustomTabs.jsx";
import {CreditCard, Deposits, PayPal, Withdraw}                from "./components";
import {pullEpicGames}                                         from "../../actions";
import {connect}                                               from 'react-redux'

class Withdrawals extends Component {
    socet = {};

    state = {
        withdrawals: [],
        deposits: [],
        withdrawalsComplete: {
            open: false,
        },
        failed: {
            open: false,
            title: '',
            text: '',
        },
        select: {_id: undefined, sum: 0, user: {name: '', password: ''}}
    };

    constructor(props) {
        super(props);
        this.handelOpen = this.handelOpen.bind(this);
        this.handelClose = this.handelClose.bind(this);
        this.handelFailed = this.handelFailed.bind(this);
        this.failedModalClose = this.failedModalClose.bind(this);
        this.handelChange = this.handelChange.bind(this);
    }

    componentDidMount() {
        const {dispatch} = this.props;
        this.socet = io();
        this.socet.on('connect', () => {
            dispatch(pullEpicGames(this.socet));
        });
        this.socet.on('withdrawals', withdrawals => {
            withdrawals = withdrawals.map(({paymentId, id, sum, status, date}) => [paymentId, id, sum * 100, status, date,
                <a onClick={() => this.handelOpen(paymentId, id, sum)}>Complete</a>]);
            this.setState({withdrawals});
        });
        this.socet.on('deposits', deposits => {
            deposits = deposits.map(({paymentId, id, sum, status, date}) => [paymentId, id, sum, status, date]);
            this.setState({deposits});
        });

    }

    handelOpen(_id, id, sum) {
        let {epicGamesAccount} = this.props;
        let user = epicGamesAccount.filter(u => u.userID === id);
        if (user.length) {
            user = user[user.length - 1];
            this.setState({
                withdrawalsComplete: {
                    open: true
                }, select: {_id, sum, user: {name: user.mail, password: user.pass}}
            });
        }
    }

    handelChange(text) {
        let {failed} = this.state;
        failed.text = text;
        this.setState({failed});

    }

    handelClose() {
        let {_id} = this.state.select;
        this.setState({
            withdrawalsComplete: {
                open: false
            }
        });
        this.socet.emit('whithdrawFailed', {_id, status: 'Done', text: ''})
    }

    handelFailed(t) {
        this.setState({
            withdrawalsComplete: {
                open: false
            },
            failed: {
                open: true,
                title: t,
                text: ''
            }
        });
    }

    failedModalClose() {
        let {text, title} = this.state.failed;
        let {_id} = this.state.select;
        let status = title === 'Why sign in failed?' ? 'Sign in failed' : 'Payment failed';
        this.setState({
            failed: {
                open: false,
                title: '',
                text: ''
            }
        });
        this.socet.emit('whithdrawFailed', {_id, status, text})
    }

    render() {

        let {deposits, withdrawals, withdrawalsComplete, failed, select} = this.state;
        return (<GridContainer>
            <Tabs
                title="Rolls:"
                headerColor="primary"
                tabs={[
                    {
                        tabName: "Withdraw",
                        tabIcon: ShopTwo,
                        tabContent: <Withdraw data={withdrawals} modal={withdrawalsComplete}
                                              modalClose={this.handelClose} onFailed={this.handelFailed}
                                              failed={failed} failedModalClose={this.failedModalClose} select={select}
                                              handelChange={this.handelChange}/>
                    },
                    {
                        tabName: "Deposits",
                        tabIcon: LocalAtm,
                        tabContent: <Deposits data={deposits}/>
                    },
                    {
                        tabName: "CreditCard",
                        tabIcon: Icon,
                        tabContent: <CreditCard/>
                    },
                    {
                        tabName: "PayPal",
                        tabIcon: MonetizationOn,
                        tabContent: <PayPal/>
                    }
                ]}
            />
        </GridContainer>);
    }
}

const mapStateToProps = state => {
    let {epicGamesAccount} = state;
    return {epicGamesAccount}
};


Withdrawals = connect(mapStateToProps)(Withdrawals);
export default Withdrawals;