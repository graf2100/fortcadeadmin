import React                  from 'react';
import PropTypes              from 'prop-types';
import {withStyles}           from '@material-ui/core/styles';
import Table                  from '@material-ui/core/Table';
import TableBody              from '@material-ui/core/TableBody';
import TableCell              from '@material-ui/core/TableCell';
import TableHead              from '@material-ui/core/TableHead';
import TablePagination        from '@material-ui/core/TablePagination';
import TableRow               from '@material-ui/core/TableRow';
import purple                 from '@material-ui/core/colors/purple';
import Modal                  from '@material-ui/core/Modal';
import BottomNavigation       from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import Typography             from '@material-ui/core/Typography';
import TableSortLabel         from '@material-ui/core/TableSortLabel';
import Checkbox               from '@material-ui/core/Checkbox';
import {lighten}              from '@material-ui/core/styles/colorManipulator';
import CardBody               from "../../../components/Card/CardBody.jsx";
import CardHeader             from "../../../components/Card/CardHeader.jsx";
import Card                   from "../../../components/Card/Card.jsx";
import Grid                   from '@material-ui/core/Grid';
import SweetAlert             from 'react-bootstrap-sweetalert';
import Button                 from '@material-ui/core/Button';
import Search                 from '@material-ui/icons/Search';
import CreditCard             from '@material-ui/icons/CreditCard';
import History                from '@material-ui/icons/History';
import io                     from 'socket.io-client';
import {pullMyBets}           from "../../../actions";
import {connect}              from "react-redux";
import TextField              from '@material-ui/core/TextField';


let counter = 0;

function createData(_id, name, deposit, history, withdrawals, game, chatBox) {
    counter += 1;
    return {id: counter, _id, name, deposit, history, withdrawals, game, chatBox};
}

function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function getSorting(order, orderBy) {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const rows = [
    {id: '_id', numeric: true, search: true, show: false, disablePadding: false, label: 'ID'},
    {id: 'name', numeric: true, search: true, show: false, disablePadding: false, label: 'Name'},
    {id: 'deposit', numeric: true, search: false, disablePadding: false, label: 'Balance'},
    {id: 'history', numeric: false, search: false, disablePadding: false, label: 'History'},
    {id: 'withdrawals', numeric: true, search: false, disablePadding: false, label: 'Withdrawals'},
    {id: 'game', numeric: true, search: false, disablePadding: false, label: 'Game'},
    {id: 'chatbox', numeric: true, search: false, disablePadding: false, label: 'Chatbox'},
];

class EnhancedTableHead extends React.Component {
    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    constructor(props) {
        super(props);
        this.state = {
            rows, values: '',
        }
    }

    showFindWindow = id => {
        let {showFind} = this.props;
        this.setState({
            rows: this.state.rows.map(row => {
                if (row.id === id) {
                    row.show = !row.show;
                } else {
                    row.show = false;
                }
                return row;
            }), values: ''
        });
        showFind({k: '', values: ''});
    };
    handelInput = e => {

    };

    render() {
        const {rows, values} = this.state;
        const {onSelectAllClick, order, orderBy, numSelected, classes, rowCount, showFind} = this.props;
        return (
            <TableHead>
                <TableRow>
                    <TableCell padding="checkbox">
                        <Checkbox
                            indeterminate={numSelected > 0 && numSelected < rowCount}
                            checked={numSelected === rowCount}
                            onChange={onSelectAllClick}
                            classes={{
                                root: classes.root,
                                checked: classes.checked,
                            }}
                        />
                    </TableCell>
                    {rows.map(row => {
                        return row.search ? (
                            <TableCell
                                key={row.id}
                                numeric={row.numeric}
                                padding={row.disablePadding ? 'none' : 'default'}
                                sortDirection={orderBy === row.id ? order : false}

                            >
                                {row.label} <Search style={{fontSize: 14}} onClick={() => this.showFindWindow(row.id)}/>
                                <div style={{
                                    display: row.show ? '' : 'none',
                                    position: 'absolute',
                                    padding: 8,
                                    borderRadius: 6,
                                    background: '#fff',
                                    boxShadow: '0 1px 6px rgba(0, 0, 0, .3)',
                                    marginLeft: '5%'
                                }}>
                                    <input type="text" value={values} placeholder={row.label}
                                           onChange={e => {
                                               this.setState({values: e.target.value});
                                               showFind({k: row.id, values: e.target.value});
                                           }}
                                           className={classes.input}/>
                                </div>
                            </TableCell>
                        ) : (
                            <TableCell
                                key={row.id}
                                numeric={row.numeric}
                                padding={row.disablePadding ? 'none' : 'default'}
                                sortDirection={orderBy === row.id ? order : false}>
                                <TableSortLabel active={orderBy === row.id} direction={order}
                                                onClick={this.createSortHandler(row.id)}>{row.label}</TableSortLabel>

                            </TableCell>
                        );
                    })}
                </TableRow>
            </TableHead>
        );
    }
}

EnhancedTableHead.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
    classes: PropTypes.object.isRequired,
    showFind: PropTypes.func.isRequired,
};
EnhancedTableHead = withStyles({
    root: {
        color: purple[400],
        '&$checked': {
            color: purple[300],
        },
    },
    checked: {},
    input: {
        fontFamily: '"Chinese Quote", -apple-system, BlinkMacSystemFont, "Segoe UI", "PingFang SC", "Hiragino Sans GB", "Microsoft YaHei", "Helvetica Neue", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
        fontVariant: 'tabular-nums',
        boxSizing: 'border-box',
        margin: 0,
        padding: 0,
        listStyle: 'none',
        position: 'relative',
        padding: '4px 11px',
        width: '100%',
        height: 32,
        fontSize: 14,
        lineHeight: 1.5,
        color: 'rgba(0, 0, 0, 0.65)',
        backgroundColor: '#fff',
        backgroundImage: 'none',
        border: '1px solid #d9d9d9',
        borderRadius: 4,
        transition: 'all .3s'
    }
})(EnhancedTableHead);

const toolbarStyles = theme => ({
    root: {
        paddingRight: theme.spacing.unit,
    },
    highlight:
        theme.palette.type === 'light'
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            }
            : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark,
            },
    spacer: {
        flex: '1 1 100%',
    },
    actions: {
        color: theme.palette.text.secondary,
    },
    title: {
        flex: '0 0 auto',
    },
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "rgba(255,255,255,.62)",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#FFFFFF"
        }
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },

});

class EnhancedTableToolbar extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {numSelected, classes, showAlert, addBalance} = this.props;
        return (
            <CardHeader color="primary">
                <Grid container spacing={24}>

                    {numSelected > 0 ? (<Grid item xs={4} style={{margin: 'auto 0'}}>
                        <h4 className={classes.cardTitleWhite}>
                            {numSelected} selected
                        </h4>
                        <p className={classes.cardCategoryWhite}>Select user</p>
                    </Grid>) : (<Grid item xs={4} style={{margin: 'auto 0'}}>
                            <h4 className={classes.cardTitleWhite}>
                                Users table
                            </h4>
                        </Grid>
                    )}
                    <Grid item xs={8} style={{textAlign: 'end', margin: 'auto 0'}}>
                        <Button onClick={() => addBalance()} className={classes.cardTitleWhite}>Add
                            balance</Button>
                        <Button className={classes.cardTitleWhite}
                                onClick={() => showAlert('withdrawals')}>Withdrawals mute</Button>
                        <Button className={classes.cardTitleWhite} onClick={() => showAlert('game')}>Game mute</Button>
                        <Button className={classes.cardTitleWhite} onClick={() => showAlert('chatbox')}>Chatbox
                            mute</Button>
                    </Grid>
                </Grid>
            </CardHeader>
        );
    }
}
;

EnhancedTableToolbar.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
    showAlert: PropTypes.func.isRequired,
    addBalance: PropTypes.func.isRequired
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
    },
    table: {
        minWidth: 1020,
    },
    tableWrapper: {
        overflowX: 'auto',
    },
    сheckbox: {
        color: purple[400],
        '&$checked': {
            color: purple[300],
        },
    },
    checked: {},
    paper: {
        position: 'absolute',
        width: '60%',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        margin: '10ch 20%'
    },
    navigation: {
        width: '100%'
    },
    navigationAction: {
        minWidth: '5rem',
        maxWidth: '100%'
    },
    modalRoot: {
        overflow: 'auto',
    }
});

class EnhancedTable extends React.Component {
    socet = {};

    constructor(props) {
        super(props);
        this.handleModalClose = this.handleModalClose.bind(this);
    }

    state = {
        order: 'asc',
        orderBy: 'calories',
        selected: [],
        data: [],
        page: 0,
        message: {
            show: false,
            type: 'game'
        },
        fillter: {
            EnhancedTable:
                ''
            ,
            values: ''
        }

        ,
        modal: {
            open: false,
            value:
                0,
            head:
                ['type', 'status', 'date', 'amount (usd)', 'amount (vbucks)', 'transactions ID'],
            body: [['']],
            id: ''
        },
        balance: {open: false, sum: 0},
        dataPay: [],
        rowsPerPage: 5,
    };

    componentDidMount() {
        const {dispatch} = this.props;
        this.socet = io();
        this.socet.on('connect', () => {
            this.socet.emit('getUsers', 1);
            dispatch(pullMyBets(this.socet));
        });
        this.socet.on('users', users => {
            let data = [];
            users.forEach(user => {
                data.push(createData(user._id, user.displayName, user.balance, '', user.disabledWithdrawal, user.disabledDeposit, user.muted));
            });
            this.setState({data})
        });
        this.socet.on('updateUser', user => {
            user = createData(user._id, user.displayName, user.balance, '', user.disabledWithdrawal, user.disabledDeposit, user.muted);
            let {data} = this.state;
            data = data.map(d => {
                if (d.name === user.name) {
                    user.id = d.id;
                    return user
                }
                return d
            });
            this.setState({data, selected: []});
        });
        this.socet.on('withdrawals', withdrawals => {
            let {dataPay} = this.state;
            dataPay = [...dataPay, ...withdrawals];
            this.setState({dataPay});
        });
        this.socet.on('deposits', deposits => {
            let {dataPay} = this.state;
            dataPay = [...dataPay, ...deposits];
            this.setState({dataPay});
        });
    }

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState({order, orderBy});
    };

    handleSelectAllClick = event => {
        if (event.target.checked) {
            this.setState(state => ({selected: state.data.map(n => n.id)}));
            return;
        }
        this.setState({selected: []});
    };

    handleClick = (event, id) => {
        const {selected} = this.state;
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];
        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        this.setState({selected: newSelected});
    };

    handleChangePage = (event, page) => {
        this.setState({page});
    };

    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    };

    isSelected = id => this.state.selected.indexOf(id) !== -1;
    handleMasage = type => {
        this.setState({
            message: {
                show: true,
                type
            }
        });

    };
    handleChange = () => {
        if (this.socet) {
            let arr = [];
            let {data, selected, message} = this.state;
            selected.forEach(sel => {
                arr.push(data.find(d => d.id === sel))
            });
            this.socet.emit('updateUsers', {type: message.type, users: arr});
            this.setState({message: {show: false}})
        }

    };

    changeBodyModal(k, id) {
        let {myBets} = this.props;
        let {modal, dataPay} = this.state;

        const parserType = t => {
            if (t === 1) {
                return 'Wood';
            } else if (t === 2) {
                return 'Llama';
            } else if (t === 3) {
                return 'Brick';
            }
        };
        if (k === 0) {
            dataPay = dataPay.filter(b => b.id === id);
            modal.body = [];
            dataPay.forEach(i => {
                modal.body.push([i.type, i.status, i.date, "$ " + i.type !== 'Deposits' ? i.sum : i.sum / 100, i.type === 'Deposits' ? i.sum : i.sum * 100, i.paymentId]);
            });
        } else if (k === 1) {
            myBets = myBets.filter(b => b.userId === id);
            modal.body = [];
            myBets.forEach(i => {
                modal.body.push([i.bet, i.winLoss ? "Win" + '(' + parserType(i.betType) + ":" + parserType(i.winType) + ")" : "Loss" + '(' + parserType(i.betType) + ":" + parserType(i.winType) + ")", i.winLoss ? "+ " + i.profit : "- " + i.profit, i.rollId, i.date])
            });
        }
        this.setState({modal});
    }

    handleModalClose() {
        let {modal} = this.state;
        modal.open = false;
        this.setState({modal})
    }

    openModel = (id, e) => {
        let {modal} = this.state;
        modal.open = true;
        modal.id = id;
        this.setState({modal});
        this.changeBodyModal(modal.value, id);
        e.preventDefault();
    };

    handleChangeModal = (event, value) => {
        let {modal} = this.state;
        modal.value = value;
        if (value === 0) {
            modal.head = ['type', 'status', 'date', 'amount (usd)', 'amount (vbucks)', 'transactions ID']
        } else {
            modal.head = ['bet amount (vbucks)', 'win/loss (bet : result)', 'PROFIT/loss', 'roll id', 'date']
        }
        this.setState({modal});
        this.changeBodyModal(value, modal.id);
    };
    addBalance = () => {
        let {balance, selected} = this.state;
        balance.open = true;
        if (selected.length) {
            this.setState({balance});
        }
    };
    handleValue = event => {
        let {balance} = this.state;
        balance.sum = event.target.value;
        this.setState({
            balance
        });
    };
    hendleAdd = e => {
        let {balance, selected, data} = this.state;
        let ids = [];
        if (balance.sum > 0) {
            selected.forEach(sel => {
                let d = data.find(d => d.id === sel);
                if (d) {
                    ids.push(d._id);
                }
            });
            this.socet.emit('addBalance', {ids, sum: balance.sum});
            balance.open = false;
            balance.sum = 0;
            this.setState({balance});
        }
        e.preventDefault();
    };


    render() {
        const {classes} = this.props;
        let {data, order, orderBy, selected, rowsPerPage, page, message, fillter, modal, balance} = this.state;
        const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);
        if (fillter.k) {
            data = data.filter(data => data[fillter.k].includes(fillter.values));
        }
        return (
            <Card>
                <SweetAlert showCancel
                            confirmBtnText="Continue"
                            confirmBtnBsStyle={'warning'}
                            type={'warning'}
                            title={`You want to enable or disable ${message.type} users!`}
                            onCancel={() => {
                                this.setState({message: {show: false}})
                            }}
                            confirmBtnStyle={{
                                padding: '10px 16px',
                                fontSize: '18px',
                                lineHeight: '1.3333333',
                                borderRadius: '6px'
                            }}
                            cancelBtnStyle={{
                                padding: '10px 16px',
                                fontSize: '18px',
                                lineHeight: '1.3333333',
                                borderRadius: '6px'
                            }}

                            onConfirm={this.handleChange} show={message.show}>
                </SweetAlert>
                <EnhancedTableToolbar numSelected={selected.length} showAlert={this.handleMasage}
                                      addBalance={this.addBalance}/>
                <CardBody>
                    <div className={classes.tableWrapper}>
                        <Table className={classes.table} aria-labelledby="tableTitle">
                            <EnhancedTableHead
                                numSelected={selected.length}
                                order={order}
                                orderBy={orderBy}
                                onSelectAllClick={this.handleSelectAllClick}
                                onRequestSort={this.handleRequestSort}
                                rowCount={data.length}
                                showFind={d => this.setState({fillter: {...d}})}
                            />
                            <TableBody>
                                {data
                                    .sort(getSorting(order, orderBy))
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map(n => {
                                        const isSelected = this.isSelected(n.id);
                                        return (
                                            <TableRow
                                                hover
                                                onClick={event => this.handleClick(event, n.id)}
                                                role="checkbox"
                                                aria-checked={isSelected}
                                                tabIndex={-1}
                                                key={n.id}
                                                selected={isSelected}
                                            >
                                                <TableCell padding="checkbox">
                                                    <Checkbox checked={isSelected} classes={{
                                                        root: classes.сheckbox,
                                                        checked: classes.checked,
                                                    }}/>
                                                </TableCell>
                                                <TableCell numeric>{n._id}</TableCell>
                                                <TableCell numeric>{n.name}</TableCell>
                                                <TableCell numeric>{n.deposit}</TableCell>
                                                <TableCell numeric><a
                                                    onClick={e => this.openModel(n._id, e)}>Open</a></TableCell>
                                                <TableCell numeric>{!n.withdrawals ? 'Enable' : 'Disable'}</TableCell>
                                                <TableCell numeric>{!n.game ? 'Enable' : 'Disable'}</TableCell>
                                                <TableCell numeric>{!n.chatBox ? 'Enable' : 'Disable'}</TableCell>
                                            </TableRow>
                                        );
                                    })}
                                {emptyRows > 0 && (
                                    <TableRow style={{height: 49 * emptyRows}}>
                                        <TableCell colSpan={6}/>
                                    </TableRow>
                                )}
                            </TableBody>
                        </Table>
                    </div>
                    <TablePagination
                        component="div"
                        count={data.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        backIconButtonProps={{
                            'aria-label': 'Previous Page',
                        }}
                        nextIconButtonProps={{
                            'aria-label': 'Next Page',
                        }}
                        onChangePage={this.handleChangePage}
                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                    />
                </CardBody>
                <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description"
                       open={modal.open} className={classes.modalRoot}
                       onClose={this.handleModalClose}>
                    <div className={classes.paper}>
                        <Typography variant="title" id="modal-title">
                            History
                        </Typography>
                        <BottomNavigation
                            value={modal.value}
                            onChange={this.handleChangeModal}
                            showLabels
                            className={classes.navigation}
                        >
                            <BottomNavigationAction label="Deposits & Withdrawals" className={classes.navigationAction}
                                                    icon={<CreditCard/>}/>
                            <BottomNavigationAction label="His bets" className={classes.navigationAction}
                                                    icon={<History/>}/>
                        </BottomNavigation>
                        <Table aria-labelledby="tableTitle">
                            <TableHead>
                                <TableRow>
                                    {modal.head.map(h => <TableCell>{h}</TableCell>)}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {modal.body.map(r => <TableRow>
                                    {r.map(i => <TableCell>{i}</TableCell>)}
                                </TableRow>)}
                            </TableBody>
                        </Table>
                    </div>
                </Modal>
                <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description"
                       open={balance.open} className={classes.modalRoot}
                       onClose={() => this.setState({balance: {open: false, sum: 0}})}>
                    <div className={classes.paper}>
                        <Typography variant="title" id="modal-title">
                            Balance
                        </Typography>
                        <form onSubmit={this.hendleAdd}>
                            <TextField
                                label="Balance"
                                type={"number"}
                                value={balance.sum}
                                onChange={this.handleValue}
                                style={{width: '100%', margin: "2rem 0"}}
                            />
                            <Button type="submit" color="primary">Send</Button>
                        </form>
                    </div>
                </Modal>
            </Card>
        );
    }
}

EnhancedTable.propTypes = {
    classes: PropTypes.object.isRequired,
    myBets: PropTypes.array,
};
const mapStateToProps = state => {
    let {myBets} = state;
    return {myBets}
};
export default connect(mapStateToProps)(withStyles(styles)(EnhancedTable));