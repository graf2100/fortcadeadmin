import Dashboard from "../layouts/Dashboard/Dashboard.jsx";
import Login     from "../layouts/Login/index.jsx";

const indexRoutes = [{path: "/login", component: Login}, {
    path: "/",
    component: Dashboard
}];

export default indexRoutes;
