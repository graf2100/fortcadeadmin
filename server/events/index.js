import EventEmitter from 'events';

class Game {
    constructor() {

        this.interval;
        this.e = new EventEmitter();
        this.cooldown = 500;
        this.previousRolls = [];
        this.futureRolls = [];
        this.events();
        this.run();
    }

    run() {
        this.interval = setInterval(() => {
            this.e.emit('next');
        }, this.cooldown)
    }

    stopRun() {
        clearInterval(this.interval);
    }

    events() {
        this.e.on('next', () => {

        });
    }
}

const game = new Game();
