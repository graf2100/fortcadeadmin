import React, {Component}                from "react";
import Table                             from "../../../components/Table/Table.jsx";
import {withStyles}                      from '@material-ui/core/styles';
import {Button, Grid, Modal, Typography} from '@material-ui/core';

import Brick from "../../../assets/img/brick.png";
import Llama from "../../../assets/img/lama.png";
import Wood  from "../../../assets/img/wood.png";

const styles = theme => ({
        paper: {
            position: 'absolute',
            width: '20%',
            backgroundColor: theme.palette.background.paper,
            boxShadow: theme.shadows[5],
            padding: theme.spacing.unit * 4,
            margin: '10ch 40%'
        },
        button: {
            margin: theme.spacing.unit,
        }
    })
;


class Future extends Component {

    state = {
        head: ['ID', 'Date', 'Result'], data: [
            ["5b5ac8d476f8e8444d52fb80", "17.09.2018 12:22:30",
                <a onClick={() => this.handleModal("5b5ac8d476f8e8444d52fb80")}>Llama</a>]
        ],
        open: false, select: ''
    };

    constructor(props) {
        super(props);

        this.handleClose = this.handleClose.bind(this);

        this.handleModal = this.handleModal.bind(this);
    }

    handleClose() {
        this.setState({open: false});
    }

    handleModal(select) {
        this.setState({open: true, select});
    }

    handleChange(type) {
        let {select} = this.state;
        console.log(select, type);

        this.setState({open: false});
    }

    render() {
        const {head, data, open} = this.state;
        const {classes} = this.props;
        let imgs = [{i: Brick, l: 'Brick'}, {i: Llama, l: 'Llama'}, {i: Wood, l: 'Wood'}];
        return <div><Table
            tableHeaderColor="rose"
            tableHead={head}
            tableData={data}
        />
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={open}
                onClose={this.handleClose}
            >
                <div className={classes.paper}>
                    <Typography variant="title" id="modal-title">
                        Change result
                    </Typography>
                    <Grid item xs={12}>
                        <Grid container
                              direction="row"
                              justify="space-around"
                              alignItems="center">
                            {imgs.map(({i, l}) => <Grid key={l} item>
                                <Button size="large" className={classes.button} onClick={() => this.handleChange(l)}>
                                    <img src={i}/>
                                </Button>
                            </Grid>)}
                        </Grid>
                    </Grid>
                </div>
            </Modal>
        </div>
    }
}

export default withStyles(styles)(Future);