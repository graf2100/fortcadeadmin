export const PULL_USER = 'PULL_USER';

export const auth = (login, pass) => {
    if (login === 'admin' && pass === 'admin') {
        return ({
            type: PULL_USER,
            data: true
        })
    } else {
        return ({
            type: PULL_USER,
            data: false
        })
    }

};

export const ADDS_USER = 'ADDS_USER';
export const QUERY_USER = 'QUERY_USER';

export const addUsers = ws => dispatch => {
    ws.emit('getUsers', 1);
    dispatch({type: QUERY_USER});
    ws.on('users', users => {

    });
};

export const PULL_ROLLS_PREVIOUS = 'PULL_ROLLS_PREVIOUS';
export const PULL_ROLLS_FUTURE = 'PULL_ROLLS_FUTURE';

export const pullPrevious = data => {
    return {type: PULL_ROLLS_PREVIOUS, data};
};

export const pullFuture = data => {
    return {type: PULL_ROLLS_FUTURE, data};
};

export const pullRoll = ws => dispatch => {
    ws.emit('getGames', 1);
    ws.on('getGames', rolls => {
        dispatch(pullPrevious(rolls));
    });
};
export const PULL_EPIC_GAMESA_CCOUNT = 'PULL_EPIC_GAMESA_CCOUNT';

export const pullEpicGames = ws => dispatch => {
    ws.on('epicGamesAccount', data => {
        dispatch({type: PULL_EPIC_GAMESA_CCOUNT, data});
    });
};
export const PULL_MYBETS = 'PULL_MYBETS';

export const pullMyBets = ws => dispatch => {
    ws.on('myBets', data => {
        dispatch({type: PULL_MYBETS, data});
    });
};