import Payment from "@material-ui/icons/Payment";
import People  from "@material-ui/icons/People";
import Restore from "@material-ui/icons/Restore";
import Users   from "../views/Users";
import Rolls   from "../views/Rolls";
import Withdrawals   from "../views/Withdrawals";

const dashboardRoutes = [
    {
        path: "/users",
        sidebarName: "Users",
        navbarName: "Users",
        icon: People,
        component: Users
    },
    {
        path: "/rolls",
        sidebarName: "Rolls",
        navbarName: "Rolls",
        icon: Restore,
        component: Rolls
    },
    {
        path: "/withdrawals",
        sidebarName: "Withdrawals",
        navbarName: "Withdrawals",
        icon: Payment,
        component: Withdrawals
    }
];

export default dashboardRoutes;
