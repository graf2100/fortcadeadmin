import React      from 'react';
import Typography from '@material-ui/core/Typography';
import Grid       from '@material-ui/core/Grid';

function EpicGames(props) {
    let {user} = props;
    return (
        <React.Fragment>
            <Grid container spacing={16}>
                <Grid item container direction="column" xs={12} sm={6}>
                    <Typography variant="title" gutterBottom>
                        User account:
                    </Typography>
                    <Grid container>
                        <React.Fragment>
                            <Grid item xs={6}>
                                <Typography variant="body2" gutterBottom>{'Login: ' + user.name}</Typography>
                                <Typography variant="body2" gutterBottom>{'Password: ' + user.password}</Typography>
                            </Grid>
                        </React.Fragment>
                    </Grid>
                </Grid>
            </Grid>
        </React.Fragment>
    );
}

export default EpicGames;