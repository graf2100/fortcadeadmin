import React, {Component}     from "react";
import withStyles             from "@material-ui/core/styles/withStyles";
import GridContainer          from "../../components/Grid/GridContainer.jsx";
import Schedule               from "@material-ui/icons/Schedule";
import AvTimer                from "@material-ui/icons/AvTimer";
import Tabs                   from "../../components/CustomTabs/CustomTabs.jsx";
import Future                 from "./Future";
import Previous               from "./Previous";
import {connect}              from 'react-redux';
import io                     from "socket.io-client";
import {pullMyBets, pullRoll} from "../../actions";

const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "rgba(255,255,255,.62)",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#FFFFFF"
        }
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    }
};

class Rolls extends Component {
    socet = {};
    state = {
        items: []
    };

    constructor(props) {
        super(props);
    }


    componentDidMount() {
        const {dispatch} = this.props;
        this.socet = io();
        this.socet.on('connect', () => {
            dispatch(pullRoll(this.socet));
            dispatch(pullMyBets(this.socet));
        });
    }

    render() {
        let {rolls, myBets} = this.props;
        return (
            <GridContainer>
                <Tabs
                    title="Rolls:"
                    headerColor="primary"
                    tabs={[
                        {
                            tabName: "Previous",
                            tabIcon: Schedule,
                            tabContent: <Previous rolls={rolls} myBets={myBets}/>
                        },
                        {
                            tabName: "Future",
                            tabIcon: AvTimer,
                            tabContent: <Future/>
                        }
                    ]}
                />
            </GridContainer>
        );
    }
}

const mapStateToProps = state => {
    let {rolls, myBets} = state;
    return {rolls, myBets}
};
Rolls = connect(mapStateToProps)(Rolls);
export default withStyles(styles)(Rolls);
