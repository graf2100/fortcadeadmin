import {combineReducers}                                                      from 'redux';
import {PULL_EPIC_GAMESA_CCOUNT, PULL_MYBETS, PULL_ROLLS_PREVIOUS, PULL_USER} from '../actions';

const authorization = (state = false, action) => {
    let {data, type} = action;
    switch (type) {
        case PULL_USER:
            return data;
        default:
            return state
    }
};

const rolls = (state = [], action) => {
    let {type, data} = action;
    switch (type) {
        case PULL_ROLLS_PREVIOUS:
            return [...data];
        default:
            return state
    }
};
const epicGamesAccount = (state = [{_id: '', winType: 1, winner: []}], action) => {
    let {type, data} = action;
    switch (type) {
        case PULL_EPIC_GAMESA_CCOUNT:
            return [...data];
        default:
            return state
    }
};
const myBets = (state = [], action) => {
    let {type, data} = action;
    switch (type) {
        case PULL_MYBETS:
            return [...data];
        default:
            return state
    }
};

const rootReducer = combineReducers({
    authorization, rolls,
    epicGamesAccount, myBets
});

export default rootReducer
