import React, {Component}                    from "react";
import Table                                 from "../../../../components/Table/Table.jsx";
import {Add, DeleteOutline, Edit}            from "@material-ui/icons";
import Button                                from "../../../../components/CustomButtons/Button.jsx";
import {Grid, Modal, Typography, withStyles} from '@material-ui/core';
import CustomInput                           from "../../../../components/CustomInput/CustomInput.jsx";
import GridItem                              from "../../../../components/Grid/GridItem.jsx";
import io                                    from "socket.io-client";
import IconButton                            from '@material-ui/core/IconButton';

const styles = theme => ({
        paper: {
            position: 'absolute',
            width: '40%',
            backgroundColor: theme.palette.background.paper,
            boxShadow: theme.shadows[5],
            padding: theme.spacing.unit * 4,
            margin: '10ch 30%'
        },
        button: {
            margin: theme.spacing.unit,
        }
    })
;

class PayPal extends Component {
    socet = {};
    state = {
        open: false,
        modal: {
            titel: 'New PayPal',
            inputs: [
                {md: 6, value: '', error: false, label: 'Email', type: 'email'},
                {md: 6, value: '', error: false, label: 'Password', type: 'password'},
            ]
        },
        cards: [], editModal: {
            _id: '',
            open: false,
            titel: 'Edit PayPal',
            inputs: [
                {md: 6, value: '', error: false, label: 'Email', type: 'email'},
                {md: 6, value: '', error: false, label: 'Password', type: 'password'},
            ]
        }
    };

    constructor(props) {
        super(props);

        this.handleClose = this.handleClose.bind(this);
        this.handleEditClose = this.handleEditClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleModal = this.handleModal.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleEditSubmit = this.handleEditSubmit.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
    }

    handleClose() {
        this.setState({open: false});
    }

    handleEditClose() {
        this.setState({
            editModal: {
                _id: '',
                open: false,
                titel: 'Edit PayPal',
                inputs: [
                    {md: 6, value: '', error: false, label: 'Email', type: 'email'},
                    {md: 6, value: '', error: false, label: 'Password', type: 'password'},
                ]
            }
        });
    }

    handleModal() {
        this.setState({
                open: true, modal: {
                    titel: 'New PayPal',
                    inputs: [
                        {md: 6, value: '', error: false, label: 'Email', type: 'email'},
                        {md: 6, value: '', error: false, label: 'Password', type: 'password'},
                    ]
                }
            }
        );
    }

    componentDidMount() {
        this.socet = io();
        this.socet.on('connect', () => {
            this.socet.emit('getPayPal', 1);
        });
        this.socet.on('payPal', payPal => {
            let cards = [];
            payPal.forEach(({email, password, _id}) => {
                cards.push([email, password,
                    <IconButton
                        onClick={() => this.handleEdit(_id, [email, password])}>
                        <Edit/>
                    </IconButton>, <IconButton
                        onClick={() => this.handleRemove(_id, [email, password])}>
                        <DeleteOutline/>
                    </IconButton>])
            });
            this.setState({cards});
        });
    }

    handleChange(e, index) {
        let {modal} = this.state;
        modal.inputs[index].value = e.target.value;
        this.setState({modal})
    };

    handleEditChange(e, index) {
        let {editModal} = this.state;
        editModal.inputs[index].value = e.target.value;
        this.setState({editModal})
    };

    handleSubmit(e) {
        if (this.checkInputs()) {
            let obj = {};
            this.state.modal.inputs.forEach(input => {
                obj = {[input.type]: input.value, ...obj}
            });
            this.socet.emit('newPayPal', obj);
            this.setState({open: false});
        }
        e.preventDefault();
    };

    handleEditSubmit(e) {
        if (this.checkEditInputs()) {
            let {editModal} = this.state;
            let obj = {};
            editModal.inputs.forEach(input => {
                obj = {[input.type]: input.value, ...obj}
            });
            this.socet.emit('editPayPal', obj, editModal._id);
            editModal.open = false;
            this.setState({editModal});
        }
        e.preventDefault();
    };

    handleEdit(id, arr) {
        let {editModal} = this.state;
        editModal.open = true;
        editModal._id = id;
        editModal.inputs = editModal.inputs.map((input, index) => {
            input.value = arr[index];
            return input
        });
        this.setState({editModal})
    };

    handleRemove(id) {
        this.socet.emit('removePayPal', id);
    };

    checkInputs() {
        let {modal} = this.state;
        let flag = true;
        modal.inputs = modal.inputs.map(input => {
            if (!input.value) {
                flag = false;
                input.error = true;
            } else {
                input.error = false;
            }
            return input;
        });
        this.setState({modal});
        return flag;
    }

    checkEditInputs() {
        let {editModal} = this.state;
        let flag = true;
        editModal.inputs = editModal.inputs.map(input => {
            if (!input.value) {
                flag = false;
                input.error = true;
            } else {
                input.error = false;
            }
            return input;
        });
        this.setState({editModal});
        return flag;
    }

    render() {
        let {open, modal, cards, editModal} = this.state;
        let {classes} = this.props;
        return (<div>
            <Button color="info" onClick={this.handleModal} round><Add/>New PayPal</Button>
            <Table
                tableHeaderColor="rose"
                tableHead={['Email', 'Password', 'Edit', 'Delete']}
                tableData={cards}
            />
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={open}
                onClose={this.handleClose}
            >
                <div className={classes.paper}>
                    <Typography variant="title" id="modal-title">
                        {modal.titel}
                        <form onSubmit={this.handleSubmit}>
                            <Grid container>
                                {modal.inputs.map((input, index) => <GridItem xs={12} sm={12} md={input.md}
                                                                              key={input.label}>
                                    <CustomInput
                                        labelText={input.label}
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        error={input.error}
                                        inputProps={{
                                            value: input.value,
                                            onChange: e => this.handleChange(e, index)
                                        }}
                                    />
                                </GridItem>)}
                                <GridItem xs={12} sm={12} md={12}>
                                    <Button type="submit" color="success">Save PayPal</Button>
                                </GridItem>
                            </Grid>
                        </form>
                    </Typography>
                </div>
            </Modal>
            <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description"
                   open={editModal.open}
                   onClose={this.handleEditClose}>
                <div className={classes.paper}>
                    <Typography variant="title" id="modal-title">
                        {editModal.titel}
                        <form onSubmit={this.handleEditSubmit}>
                            <Grid container>
                                {editModal.inputs.map((input, index) => <GridItem xs={12} sm={12} md={input.md}
                                                                                  key={input.label}>
                                    <CustomInput
                                        labelText={input.label}
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        error={input.error}
                                        inputProps={{
                                            value: input.value,
                                            onChange: e => this.handleEditChange(e, index)
                                        }}
                                    />
                                </GridItem>)}
                                <GridItem xs={12} sm={12} md={12}>
                                    <Button type="submit" color="success">Save PayPal</Button>
                                </GridItem>
                            </Grid>
                        </form>
                    </Typography>
                </div>
            </Modal>
        </div>);
    }
};
PayPal = withStyles(styles)(PayPal);
export default PayPal;