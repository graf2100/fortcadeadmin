import React, {Component}              from "react";
import Table                           from "../../../components/Table/Table.jsx";
import {Modal, Typography, withStyles} from '@material-ui/core';
import Llama                           from "../../../assets/img/lama.png";
import moment                          from "moment";

const styles = theme => ({
        paper: {
            position: 'absolute',
            width: '40%',
            backgroundColor: theme.palette.background.paper,
            boxShadow: theme.shadows[5],
            padding: theme.spacing.unit * 4,
            margin: '10ch 30%'
        },
        button: {
            margin: theme.spacing.unit,
        }
    })
;


class Previous extends Component {

    state = {
        head: ['ID', 'Date', 'Bets', 'Result', 'Profit'],
        data: [["5b5ac8d476f8e8444d52fb80", "17.09.2018 12:22",
            <a onClick={() => this.handleModal("5b5ac8d476f8e8444d52fb80")}>Open</a>, "Llama", "$36"]],
        open: false,
        tableData: []
    };

    constructor(props) {
        super(props);

        this.handleClose = this.handleClose.bind(this);

        this.handleModal = this.handleModal.bind(this);
    }

    handleClose() {
        this.setState({open: false});
    }

    handleModal(select) {
        let {rolls} = this.props;
        let tableData = [];
        rolls = rolls.find(roll => roll._id === select).rates;
        if (rolls) {
            let s = (r, t) => r.filter(roll => roll.betType === t).map(roll => roll.userId + "-" + roll.bet + "Vb");
            s(rolls, 1).forEach(i => {
                tableData.push([i, '', '']);
            });
            s(rolls, 2).forEach((a, i) => {
                if (tableData[i])
                    tableData[i][1] = a;
            });
            s(rolls, 3).forEach((a, i) => {
                if (tableData[i])
                    tableData[i][2] = a;
            });
        }
        this.setState({open: true, select, tableData});
    }

    chakeType(type) {
        switch (type) {
            case 1: {
                return 'WOOD'
            }
            case 2: {
                return 'LLAMA'
            }
            case 3: {
                return 'BRICK'
            }
        }
    }

    mathProfit(arr, type) {
        let sum = 0;
        if (arr) {
            arr.forEach(a => {
                if (type === a.type) {
                    sum += a.rate * 0.2;
                } else {
                    sum += a.rate;
                }
            });
        }
        return Math.round(sum) + " Vb";
    }

    render() {
        let {head, open, tableData} = this.state;
        let data = [];
        let {classes, rolls} = this.props;
        rolls.forEach(roll => {
            data.push([roll._id, moment(roll.date).format('MMM Do, h:mm:ss'),
                <a onClick={() => this.handleModal(roll._id)}>Open</a>, this.chakeType(roll.winType), this.mathProfit(roll.winner, roll.winType)]);
        });

        return <div><Table
            tableHeaderColor="rose"
            tableHead={head}
            tableData={data}
        />
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={open}
                onClose={this.handleClose}
            >
                <div className={classes.paper}>
                    <Typography variant="title" id="modal-title">
                        Bets
                    </Typography>
                    <Table
                        tableHeaderColor="rose"
                        tableHead={['Wood', 'Llama', 'Brick',]}
                        tableData={tableData}
                    />
                </div>
            </Modal>
        </div>
    }
}

export default withStyles(styles)(Previous);