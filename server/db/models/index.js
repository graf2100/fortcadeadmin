const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;
mongoose.connect('mongodb://localhost/fortcade', {useNewUrlParser: true});

const userSchema = new Schema({
    _id: {type: String},
    googleID: {type: String},
    displayName: {type: String},
    avatar: {type: String},
    email: {type: String},
    balance: {type: Number, default: 0, min: 0},
    muted: {type: Boolean, default: true},
    mutedToDate: {type: Date, default: new Date()},
    disabledDeposit: {type: Boolean, default: false},
    disabledDepositToDate: {type: Date, default: 0},
    disabledWithdrawal: {type: Boolean, default: false},
    disabledWithdrawalToDate: {type: Date, default: 0},
}, {
    timestamps: true
});
const paymentsSchema = new Schema({
    _id: {type: String},
    id: {type: String},
    paymentId: {type: String},
    sum: {type: Number, default: 0, min: 0},
    type: {type: String},
    status: {type: String},
    date: {type: String}
});
const gamesSchema = new Schema({
    _id: {type: ObjectId},
    winType: {type: Number},
    winner: {type: Array},
    rates: {type: Array},
    date: {type: Date},
});


const rollsSchema = new Schema({
    date: {type: Date, default: Date.now},
    bets: {type: [{userId: String, userName: String, bets: [{sum: Number, type: String}]}], default: []},
    result: {type: Number, default: 0},
    profit: {type: Number, default: 0},
    status: {type: String, default: 'Future'}
});
const creditCardSchema = new Schema({
    first: String,
    last: String,
    address: String,
    city: String,
    province: String,
    zip: String,
    card: String,
    cvv: String,
    expiry: String,
    activation: String,
});
const epicGamesAccountSchema = new Schema({
    userID: String,
    mail: String,
    pass: String,
    _v: Number
});
const PayPalSchema = new Schema({
    email: String,
    password: String,
});

const myBetsSchema = new Schema({
    bet: Number,
    betType: Number,
    winLoss: Boolean,
    winType: Number,
    rollId: ObjectId,
    profit: Number,
    date: String,
    userId: String
}, {collection: 'myBets'});


let Users = mongoose.model('users', userSchema);
let Payments = mongoose.model('payments', paymentsSchema);
let Games = mongoose.model('games', gamesSchema);
let Rolls = mongoose.model('rolls', rollsSchema);
let CreditCard = mongoose.model('creditCard', creditCardSchema);
let epicGamesAccount = mongoose.model('epicGamesAccount', epicGamesAccountSchema);
let myBets = mongoose.model('myBets', myBetsSchema);
let PayPal = mongoose.model('PayPal', PayPalSchema);

module.exports = {Payments, Users, Games, Rolls, CreditCard, epicGamesAccount, myBets, PayPal};