const {Users, Payments, Games, CreditCard, epicGamesAccount, myBets, PayPal} = require('./db');
const app = require('express')();
const express = require('express');
const http = require('http').Server(app);
const io = require('socket.io')(http);
const path = require('path');

app.use(express.static(path.join(__dirname, '..', 'build')));
app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, '..', 'build', 'index.html'));
});

let findUser = () => {
    Users.find({}, (err, data) => {
        io.emit('users', data);
    });
};
io.on('connection', function (socket) {

    myBets.find({}).exec().then(data => {
        socket.emit("myBets", data);
    });

    socket.on('getUsers', findUser);

    socket.on('getGames', () => {
        Games.find({}).limit(100).exec((err, data) => {
            io.emit('getGames', data);
        });
    });
    socket.on('getRolls', () => {
        Games.find({}).limit(100).exec((err, data) => {
            io.emit('getRolls', data);
        });
    });
    socket.on('getCreaditCart', () => {
        CreditCard.find({}).exec((err, data) => {
            io.emit('creaditCart', data);
        });
    });
    socket.on('whithdrawFailed', ({_id, status, text}) => {
        Payments.updateOne({paymentId: _id}, {status}, () => {
            Payments.find({type: "Withdraw"}, (err, data) => {
                socket.emit('withdrawals', data);
            });
        });
    });
    socket.on('newCreaditCart', card => {
        let Card = new CreditCard(card);
        Card.save(e => {
            if (e) {
                console.error(e);
            }
            CreditCard.find({}).exec((err, data) => {
                io.emit('creaditCart', data);
            });
        })
    });
    socket.on('removeCreaditCart', _id => {
        CreditCard.deleteOne({_id}, () => {
            CreditCard.find({}).exec((err, data) => {
                io.emit('creaditCart', data);
            });
        });
    });
    socket.on('editCreaditCart', (obj, _id) => {
        CreditCard.updateOne({_id}, {...obj}, () => {
            CreditCard.find({}).exec((err, data) => {
                io.emit('creaditCart', data);
            });
        });
    });
    socket.on('getPayPal', () => {
        PayPal.find({}).exec((err, data) => {
            io.emit('payPal', data);
        });
    });
    socket.on('newPayPal', card => {
        let payPal = new PayPal(card);
        payPal.save(e => {
            if (e) {
                console.error(e);
            }
            PayPal.find({}).exec((err, data) => {
                io.emit('payPal', data);
            });
        })
    });
    socket.on('removePayPal', _id => {
        PayPal.deleteOne({_id}, () => {
            PayPal.find({}).exec((err, data) => {
                io.emit('payPal', data);
            });
        });
    });
    socket.on('editPayPal', (obj, _id) => {
        PayPal.updateOne({_id}, {...obj}, () => {
            PayPal.find({}).exec((err, data) => {
                io.emit('payPal', data);
            });
        });
    });
    socket.on('addBalance', ({ids, sum}) => {
        ids.forEach(id => {
            Users.findOne({_id: id}).then(doc => {
                doc.balance += parseInt(sum);
                doc.save();
                io.emit('updateUser', doc);
            });
        });
    });
    socket.on('updateUsers', ({type, users}) => {
        users.forEach(user => {
            Users.findOne({_id: user._id}, function (err, doc) {
                switch (type) {
                    case 'withdrawals': {
                        doc.disabledWithdrawal = !doc.disabledWithdrawal;
                        doc.save();
                        break;
                    }
                    case 'game': {
                        doc.disabledDeposit = !doc.disabledDeposit;
                        doc.save();
                        findUser();
                        break;
                    }
                    case 'chatbox': {
                        doc.muted = !doc.muted;
                        doc.save();
                        break;
                    }
                }
                io.emit('updateUser', doc);
            });
        })
    });

    epicGamesAccount.find({}, (err, data) => {
        socket.emit('epicGamesAccount', data);
    });
    Payments.find({type: "Withdraw"}, (err, data) => {
        socket.emit('withdrawals', data);
    });
    Payments.find({type: "Deposits"}, (err, data) => {
        socket.emit('deposits', data);
    });

})
;

http.listen(8090, function () {
    console.log('listening on *:8090');
});