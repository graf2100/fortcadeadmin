import React, {Component, Fragment}                                                     from 'react';
import {FormControl, Grid, Input, InputLabel, MenuItem, Select, Typography, withStyles} from '@material-ui/core';
import io                                                                               from "socket.io-client";

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: '100%',

    },
    selectEmpty: {
        marginTop: theme.spacing.unit,
    },
});

class Payment extends Component {
    socet = {};
    state = {
        select: -1,
        selectPayPal: -1,
        cards: [],
        card: {
            first: '',
            last: '',
            address: '',
            city: '',
            province: '',
            zip: '',
            card: '',
            cvv: '',
            expiry: '',
            activation: ''
        },
        pay: {
            email: '',
            password: ''
        },
        payPal: []
    };
    handleChange = event => {
        let {cards} = this.state;
        this.setState({select: event.target.value, card: cards[event.target.value]});
    };
    handleChangePayPal = event => {
        let {payPal} = this.state;
        this.setState({selectPayPal: event.target.value, pay: payPal[event.target.value]});
    };

    componentDidMount() {
        this.socet = io();
        this.socet.on('connect', () => {
            this.socet.emit('getCreaditCart', 1);
            this.socet.emit('getPayPal', 1);
        });
        this.socet.on('creaditCart', cards => {
            this.setState({cards});
        });
        this.socet.on('payPal', payPal => {
            this.setState({payPal});
        });
    }


    render() {
        let {classes, sum} = this.props;
        let {cards, card, payPal, pay} = this.state;
        let {first, last, address, city, province, zip, cvv, expiry, activation} = card;
        let {email, password} = pay;
        return (
            <Fragment>
                <Typography variant="title" gutterBottom>
                    Payment: 1000 V-Buks - {sum} $
                </Typography>
                <Grid container spacing={24} style={{marginTop: 15}}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="CreditCard-helper">CreditCard</InputLabel>
                        <Select value={this.state.select} onChange={this.handleChange}
                                input={<Input name="CreditCard"/>}>
                            {cards.map((card, index) => <MenuItem key={card._id} value={index}>{card.card}</MenuItem>)}
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item container direction="column" xs={12} sm={6}>
                    <Grid container style={{marginTop: 10}}>
                        {first &&
                        <Grid item xs={6}>
                            <Typography gutterBottom>First name: {first}</Typography>
                            <Typography gutterBottom>Last name: {last}</Typography>
                            <Typography gutterBottom>Address: {address}</Typography>
                            <Typography gutterBottom>City: {city}</Typography>
                            <Typography gutterBottom>Province: {province}</Typography>
                            <Typography gutterBottom>Zip: {zip}</Typography>
                            <Typography gutterBottom>Cvv: {cvv}</Typography>
                            <Typography gutterBottom>Expiry: {expiry}</Typography>
                            <Typography gutterBottom>Activation: {activation}</Typography>
                        </Grid>}
                    </Grid>
                </Grid>
                <Grid container spacing={24} style={{marginTop: 15}}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="CreditCard-helper">PayPal</InputLabel>
                        <Select value={this.state.selectPayPal} onChange={this.handleChangePayPal}
                                input={<Input name="CreditCard"/>}>
                            {payPal.map((card, index) => <MenuItem key={card._id}
                                                                   value={index}>{card.email}</MenuItem>)}
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item container direction="column" xs={12} sm={6}>
                    <Grid container style={{marginTop: 10}}>
                        {email &&
                        <Grid item xs={6}>
                            <Typography gutterBottom>Email: {email}</Typography>
                            <Typography gutterBottom>Password: {password}</Typography>
                        </Grid>}
                    </Grid>
                </Grid>
            </Fragment>
        );
    }
}

export default withStyles(styles)(Payment);